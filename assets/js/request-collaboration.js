$(function() {
    $("form#collaboration").validate()
});

$("form#collaboration").submit(function( event ) {
  event.preventDefault();
  if (!$('form#collaboration').valid()) {
    return;
  }

  airtable.submitCollaborationRequest(
    new FormData(this)
  ).done(function (data, url) {
    console.log(`successfully posted one collaboration request to ${url}`);
    alert("Thank you! Our team will be in touch with you shortly.")
    window.location.replace("/")
  })
  .fail(function() {
    alert("Oops! Something went wrong.... Please try again later, or email us at contact@covidcp.org if the problem persists.")
  });
});

function toggleTrialTypeDetails(value) {
  if (value === "specific trial") {
    $('#trial-interest-group').hide();
    $('#trial-interest').removeAttr("required");
    $('#protocol-title-group').show();
    $('#protocol-title').attr("required", "");
  } else {
    $('#trial-interest-group').show();
    $('#trial-interest').attr("required", "");
    $('#protocol-title-group').hide();
    $('#protocol-title').removeAttr("required");
  }
}

$(document).ready(function() {
  if (request.params['title']) {
    $('#protocol-title').val(decodeURIComponent(request.params['title']))
    $('#protocol-title').height($('#protocol-title')[0].scrollHeight);
  }

  // If the protocol id is provided, then select the specific trial radio option,
  // otherwise select the general interest radio option.
  if (request.params['protocol-id']) {
    $('#protocol-id').val(request.params['protocol-id']);
    $('#specific-trial').prop('checked', true);
    toggleTrialTypeDetails("specific trial");
  } else {
    $('#general-interest').prop('checked', true);
    toggleTrialTypeDetails("general interest");
  }

  $("input[name='Collaboration Type']").change(e => {
    toggleTrialTypeDetails(e.target.value);
  });
});

import os
import random
import boto3

from flask import Flask, request, jsonify
from dotenv import load_dotenv
from airtable import Airtable
from flask_cors import CORS
from logging.config import dictConfig
from cachetools.func import ttl_cache
from distutils.util import strtobool

load_dotenv()
BASE_ID                     = os.getenv('AIRTABLE_BASE_ID')
API_KEY                     = os.getenv('AIRTABLE_API_KEY')
PROTOCOL_TABLE              = os.getenv('AIRTABLE_PROTOCOL_TABLE_NAME')
COLLABORATION_REQUEST_TABLE = os.getenv('AIRTABLE_COLLABORATION_REQUEST_TABLE_NAME')

WHITELISTED_FIELDS = [
    'ID',
    'Title',
    'Study Type',
    'PI Name', 'PI Institution',
    'Co-PI Name', 'Co-PI Institution',
    'Status',
    'NCT ID',
    'City', 'State', 'Country',
    'Eligibility Criteria',
    'Intervention Arms',
    'Standard Outcomes',
    'Custom Outcomes',
    'Notes',
    'Collaborating Protocols',
    'Title (from Collaborating Protocols)',
    'Title (from Collaborating External Trials)',
    'Link (from Collaborating External Trials)',
    'Share Patient Data',
    'Categories',
    '__created_at__',
]

ATTACHMENT_FIELDS = ['Attachment', 'Patient Consent Forms', 'Case Report Form', 'Data Dictionary']

logfile = '/opt/python/log/application.log'
try:
    with open(logfile, 'w'):
        pass
except:
    logfile = os.path.join(os.path.expanduser('~'), 'application.log')

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': u'[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://flask.logging.wsgi_errors_stream',
            'formatter': 'default'
        },
        'file': {
            'class': 'logging.FileHandler',
            'filename': logfile,
            'formatter': 'default',
            'encoding': 'utf-8',
        }
    },
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi', 'file'],
    }
})


application = Flask(__name__)
CORS(application)

s3 = boto3.resource('s3')
bucket = s3.Bucket('protocol-uploads')


@application.route('/status', methods=['GET'])
def status():
    application.logger.info('Status checked')
    return jsonify({'status': 'OK'})

def uploadFile(name, protocol):
    attachment = request.files[name]
    if attachment.filename:
        rkey = str(random.getrandbits(2**6))
        saveas = '{}{}'.format(rkey, attachment.filename)
        bucket.upload_fileobj(attachment.stream, saveas)
        bucket_url = 'https://protocol-uploads.s3.us-east-2.amazonaws.com/{}'
        url = bucket_url.format(saveas)
        protocol[name] = [{
            'url': url,
            'filename': attachment.filename,
        }]

@application.route('/protocols', methods=['POST'])
def submit():
    VECTOR_FIELDS = ['Standard Outcomes']
    protocol = dict(
        (k, v) if k in VECTOR_FIELDS
        else (k, v.pop())
        for k, v in dict(request.form.lists()).items()
    )
    protocol['Approved'] = False

    for field in ['Show Attachment',
                  'Share Patient Data',
                  'Show Patient Consent Forms',
                  'Show Case Report Form',
                  'Show Data Dictionary']:
        protocol[field] = bool(strtobool(protocol.get(field, 'false')))

    for field in ATTACHMENT_FIELDS:
        uploadFile(field, protocol)

    airtable = Airtable(BASE_ID, PROTOCOL_TABLE, api_key=API_KEY)
    application.logger.info('inserting protocol: {}'.format(protocol))
    try:
        record = airtable.insert(protocol)
    except Exception as e:
        application.logger.exception('Failed to insert record.\n')
        raise e
    return jsonify(**record)


@application.route('/collaboration-requests', methods=['POST'])
def submit_collaboration():
    collaboration = request.form.to_dict()
    airtable = Airtable(BASE_ID, COLLABORATION_REQUEST_TABLE, api_key=API_KEY)
    application.logger.info('inserting collaboration: {}'.format(collaboration))
    try:
        record = airtable.insert(collaboration)
    except Exception as e:
        application.logger.error('Failed to insert record.\n')
        raise e
    return jsonify(**record)


@application.route('/protocols/<pid>', methods=['GET'])
@ttl_cache(ttl=60)
def protocol(pid):
    airtable = Airtable(BASE_ID, PROTOCOL_TABLE, api_key=API_KEY)
    record = airtable.get(pid)
    if not record['fields'].get('Approved', False):
        return "Cannot view unapproved protocol", 403
    application.logger.info('returning {}'.format(record))
    return jsonify(_whitelist(record))


@application.route('/protocols', methods=['GET'])
@ttl_cache(ttl=60)
def protocols():
    airtable = Airtable(BASE_ID, PROTOCOL_TABLE, api_key=API_KEY)

    try:
        records = [
            _whitelist(record) for record in
            airtable.get_all(formula="Approved", maxRecords=10000)
        ]

    except Exception as e:
        application.logger.exception('Failed to retrieve records.\n')
        raise e
    application.logger.info('returning {} protocols'.format(records))
    return jsonify(records=records)


def _whitelist(record):
    whitelisted_fields = WHITELISTED_FIELDS.copy()

    for field in ATTACHMENT_FIELDS:
        if record['fields'].get('Show ' + field, False):
            whitelisted_fields.append(field)

    record['fields'] = {
        k: v for k, v in record['fields'].items()
        if k in whitelisted_fields
    }
    return record


if __name__ == '__main__':
    application.run()

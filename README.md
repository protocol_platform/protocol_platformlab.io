![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

# Link Styling

By default, links do not get underlines. If you want an underline for a
particular link, add class="important-link".

# GitLab CI

### Static site

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml). The site is currently configured
to be deployed on push to master.

### Previewing merge requests

Merge requests can be previewed by clicking the "view app" button in the merge
request overview.

### Server

The server is currently deployed on amazon elastic beanstalk, and is redeployed
each time there is a push or merge to the master branch.

# Development

### Serve Static Site locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. [Install][hugo] Hugo
3. Preview your project: `hugo server`
4. Add content
5. Generate the website: `hugo` (optional)

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

### Run Server Locally

The server lives in the [server](server) directory. To run it locally: 

1. Make sure the requirements listed in
[server/requirements.txt](server/requirements.txt) are satisfied (e.g. through
`pip install -r requirements.txt`).
2. In the [server](server) directory, copying `.env.sample` to `.env`,
   which is used for local configuration. Fill in the appropriate values in 
   `.env`, which you can find by logging into our Airtable account on the web 
   and then browsing to https://airtable.com/api.
3. Run the server with `python application.py` from inside the server directory.

The server will now be running on your localhost, at a port specified in the
readout from that last command (usually `5000`, except in case of conflict).

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains

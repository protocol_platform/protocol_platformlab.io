---
title: Data pooling
---

# Ongoing data pooling efforts

For more information about ongoing efforts to pool patient level data for aggregated safety and efficacy
analyses, see the links below. (If you have information about other similar efforts please email 
<a href="mailto:contact@covidcp.org">contact@covidcp.org</a>
so that we can link to them here.)

- [Convalescent plasma trials](/convalescent-plasma)
- [Hydroxycloroquine trials](/hydroxychloroquine)

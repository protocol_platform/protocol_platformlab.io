---
title: Frequently Asked Questions
---

### How is CovidCP different from clinicaltrials.gov and other trial registries?

Many registries catalogue COVID-19 trials, but they do not actively support and encourage collaboration.
We are not a registry, we are a repository of only those COVID-19 RCTs that are open for collaboration.
And we provide support for collaboration through vetting requests, IRB and TIN support, statistical support,
and our data-sharing partnership with Vivli.

<br>

### How is CovidCP different from other COVID-related data-sharing initiatives?

Most other initiatives, including CD2H/N3C and many others around the world, differ from CovidCP in two ways.
 First, they are repositories of data to be harmonized ex post facto while CovidCP seeks to create
 collaborations and encourage coordination and harmonization before studies are completed.  Second, these
 initiatives are mainly--though not exclusively--focused on observational and EMR data, while we are primarily
 focused on randomized trials.

<br>

### What about SOLIDARITY, REMAP-CAP, and other large multisite trial initiatives?

It is heartening that some of the most important clinical questions are being studied in the context of large
scale coordinated efforts like these.  Our primary goal is to capture the clinical questions that are not
already part of national or global efforts like these--including questions about some therapeutics, most
non-therapeutic interventions, and many outpatient treatment and prevention strategies. 

<br>

### What about ACTIV?

ACTIV is a partnership between NIH and pharmaceutical companies; it does not seem like it will include academic research.  It will focus on a group of (yet-to-be-determined) drug and vaccine interventions, so it will not cover all interventions (i.e. it will not include all drugs, it will not include non-pharmaceutical interventions or prevention measures).  

<br>

### Who can see my protocol and contact information if I submit my trial to CovidCP?

Your contact information will be behind a request wall.  We will vet all collaboration requests to ensure that that they have legitimate research purposes, and only then will we put you directly in touch with the requesting researcher(s).  When you submit your protocol to CovidCP you have the option of putting the full-text protocol behind this same request wall, but the default is that it will be visible to all on the website.   

<br>

### How do I contact other researchers whose protocols are on CovidCP?
You can ask to collaborate on a protocol <a class="priority"
            href="https://www.covidcp.org/request-collaboration/"
            target="_blank">
            here</a>.
We vet such requests for legitimate scientific value, i.e. to weed out spam, and will put you in touch with
the researchers responsible for the protocol.

<br>

### Are industry-sponsored protocols accepted as well as investigator-initiated?
Yes. We hope that all kinds of researchers from all corners of the research community will collaborate.

<br>

### I would like to take my protocol down so that it’s no longer accessible through the CovidCP database. How do I go about doing this?
Email contact@covidcp.org with “remove protocol” in the subject line and we will take it down.

<br>

### What do I do if there are changes to the research protocol that I’ve already submitted to CovidCP
Email contact@covidcp.org and we can help you update it.

<br>

### How do I share my data with other researchers?
We are grateful to <a class="priority"
            href="https://vivli.org/vivli-covid-19-portal-2/"
            target="_blank">
            Vivli</a>
            for providing free anonymization and data storage to CovidCP participants.

<br>

### Does my data need to be anonymized before I upload it onto the Vivli platform?
No, Vivli and their partners will anonymize your data for you.

<br>

### Is there a cost associated with sharing data on Vivli?
No, it’s entirely free.




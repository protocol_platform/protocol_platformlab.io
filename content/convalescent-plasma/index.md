---
title: Convalescent Plasma Trials
layout: 'category'
type: 'page'
category: 'convalescent plasma'
---

<p>If you are interested in our effort to pool individual patient data from pediatric convalescent plasma trials,
please email Marianne Gildea at <a href="mailto:mgildea1@jhmi.edu">mgildea1@jhmi.edu </a>
or <a href="mailto:contact@covidcp.org">contact@covidcp.org</a>.</p>

<p>For more information about the COMPILE project to pool individual patient data from inpatient adult convalescent plasma trials,
click <a href="http://nyulmc.org/compile">here</a>.</p>

<p>If you are interested our effort to pool individual patient data from outpatient adult convalescent plasma trials,
please email Marianne Gildea at <a href="mailto:mgildea1@jhmi.edu">mgildea1@jhmi.edu </a>
or <a href="mailto:contact@covidcp.org">contact@covidcp.org</a>.</p>

<p>Here are the Convalescent Plasma trials that are currently registered with us:</p>

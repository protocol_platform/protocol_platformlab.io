---
title: Hydroxychloroquine Trials
layout: 'category'
type: 'page'
category: 'hydroxychloroquine'
---

<p>We are currently working with 16 therapeutic hydroxychlroquine trials to pool individual 
patient data for inpatient and outpatient safety and efficacy analyses. If you are interested
in sharing data from a therapeutic hydroxychlroquine trial, please email 
<a href="mailto:eogburn@jhsph.edu">eogburn@jhsph.edu</a> or <a href="mailto:contact@covidcp.org">contact@covidcp.org</a>. </p>


<p>Here are the Hydroxychloroquine trials that are currently registered with us:</p>

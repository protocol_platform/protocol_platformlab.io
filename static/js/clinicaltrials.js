fields = {
  'title': [
    p => p.IdentificationModule.OfficialTitle
  ],

  'brief-title': [
    p => p.IdentificationModule.BriefTitle
  ],

  'eligibility': [
    p => p.EligibilityModule.EligibilityCriteria
  ],

  'pi-name': [
    p => p.ContactsLocationsModule.OverallOfficialList.OverallOfficial[0].OverallOfficialName
  ],

  'pi-institution':  [
    p => p.ContactsLocationsModule.OverallOfficialList.OverallOfficial[0].OverallOfficialAffiliation
  ],

  'co-pi-name': [
    p => p.ContactsLocationsModule.OverallOfficialList.OverallOfficial[1].OverallOfficialName
  ],

  'co-pi-institution': [
    p => p.ContactsLocationsModule.OverallOfficialList.OverallOfficial[1].OverallOfficialAffiliation
  ],

  'city': [
    p => p.ContactsLocationsModule.LocationList.Location[0].LocationCity
  ],

  'state': [
    p => p.ContactsLocationsModule.LocationList.Location[0].LocationState
  ],

  'country': [
    p => p.ContactsLocationsModule.LocationList.Location[0].LocationCountry
  ],

  'intervention-arms': [
    p => p.ArmsInterventionsModule.InterventionList.Intervention.map(i =>
      `${i.InterventionName} [${i.InterventionDescription}]`
    ).join("\n\n")
  ],

  'custom-outcomes': [
    p => p.OutcomesModule.PrimaryOutcomeList.PrimaryOutcome.map(o =>
      `${o.PrimaryOutcomeMeasure} [${o.PrimaryOutcomeTimeFrame}]`
    ).join("\n\n"),

    p => p.OutcomesModule.SecondaryOutcomeList.SecondaryOutcome.map(o =>
      `${o.SecondaryOutcomeMeasure} [${o.SecondaryOutcomeTimeFrame}]`
    ).join("\n\n"),

    p => p.OutcomesModule.OtherOutcomeList.OtherOutcome.map(o =>
      `${o.OtherOutcomeMeasure} [${o.OtherOutcomeTimeFrame}]`
    ).join("\n\n"),
  ]
}

clinicaltrials = {
  getProtocol: function (nctId) {
    url = `https://clinicaltrials.gov/api/query/full_studies?expr=${nctId}&field=NCTId&fmt=JSON`;
    console.log(`GET ${url}`)

    return $.getJSON(url, function(data) {})
      .then(function(data) {
        if (data.FullStudiesResponse.NStudiesFound == 0) {
          return null
        } else {
          protocol = data.FullStudiesResponse.FullStudies[0].Study.ProtocolSection

          return Object.entries(fields).reduce( (values, pair) => {
            const [id, paths] = pair

            items = paths.reduce( (_values, path) => {
              try {
                _values.push(path(protocol))
              }
              catch {
                console.log(`clinicaltrials.gov: could not find protocol value for '${path}'`);
              }
              return _values
            }, []);

            if (items) {
              values[id] = items.join("\n\n")
            }
            return values

          }, {});
        }
      });
  }
}

const request = {
  get params() {
    return (window.location.href.match(/([^?&]+=[^&]+)/g) || [])
      .map(param => param.replace("_", "-"))
      .map(param => param.split('='))
      .map(([key, value]) => [key.toLowerCase(), value])
      .reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {});
  }
}

const airtableServiceEndpoint = $("meta[name='airtable-endpoint']").attr("content");

if (!airtableServiceEndpoint) {
  throw "Could not detect airtable endpoint"
}

function post(url, formData, timeout=10000) {
  console.log(`posting one new record to ${url}`)
  return $.ajax({
    type:        "POST",
    url:         url,
    data:        formData,
    dataType:    'json',
    contentType: false,
    processData: false,
    timeout:     timeout  // required to catch connection errors
  }).then( function (data) {
    return [data, url]
  });
}

airtable = {

  getProtocol: function (pid) {
    return $.get(`${airtableServiceEndpoint}/protocols/${pid}`).then(record => {
      return record.fields
    })
  },

  getProtocols: function () {
    return $.get(`${airtableServiceEndpoint}/protocols`).then(response => {
      return response.records.map(r => r.fields)
    })
  },

  submitProtocol: function (formData) {
    return post(`${airtableServiceEndpoint}/protocols`, formData, timeout=40000);
  },

  submitCollaborationRequest: function (formData) {
    return post(`${airtableServiceEndpoint}/collaboration-requests`, formData);
  },

}

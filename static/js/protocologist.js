MIN_QUERY_CHARS = 3

const FUSE_OPTIONS = {
  // See https://fusejs.io/api/options.html
  isCaseSensitive:    false,
  includeScore:       false,
  includeMatches:     true,
  minMatchCharLength: MIN_QUERY_CHARS,
  shouldSort:         true,
  threshold:          0.2, // 0 = exact match, 1 = anything
  keys: [
    'Title', 'NCT ID',
    'PI Name', 'PI Institution',
    'Co-PI Name', 'Co-PI Institution',
    'Standard Outcomes', 'Custom Outcomes',
    'Eligibility Criteria', 'Intervention Arms'
  ]
}

const thisMany = protocols =>
  `${protocols.length} ${protocols.length == 1 ? 'protocol' : 'protocols'}`

var protocologist = {
  _ttlInSeconds: 60,
  _updatedAt:    undefined,
  _protocols:    undefined,

  get _needsRefresh() {
    if (!protocologist._updatedAt) return true;
    if (!protocologist.protocols) return true;
    return ((new Date() - protocologist._updatedAt) / 1000) > protocologist._ttlInSeconds;
  },

  get protocols() {
    return protocologist._protocols;
  },

  set protocols(_protocols) {
    protocologist._protocols = _protocols
    protocologist._updatedAt = new Date()
  },

  get: (pid, callback) => {
    airtable.getProtocol(pid).done(callback)
  },

  filter: (query, filter, callback) => {
    protocologist.getAll(allProtocols => {
      matchingProtocols = allProtocols

      if (query.text.length > MIN_QUERY_CHARS) {
        options = {...FUSE_OPTIONS, ...query.options}
        var fuse = new Fuse(matchingProtocols, options)
        matchingProtocols = fuse.search(query.text).map(x => x.item)
      }

      for (let [field_name, values] of Object.entries(filter)) {
        if (values.length > 0) {
          matchingProtocols = matchingProtocols.filter(p =>
            [(p[field_name] || [])].flat().filter(o =>
              values.includes(o)
            ).length > 0
          )
        }
      }

      callback(matchingProtocols)
    });
  },

  filterByCategory: (category, callback) => {
    protocologist.getAll(allProtocols => {
      callback(allProtocols.filter(p => (p['Categories'] || []).includes(category)))
    })
  },

  getAll: callback => {
    if (protocologist._needsRefresh) {
      console.log("fetching protocols")
      airtable.getProtocols()
        .then(protocols => {
          console.log(`fetched ${protocols.length} protocols`)
          protocologist.protocols = protocols
          return protocols
        })
        .done(callback)
        .fail(() => {
          callback(protocologist.protocols)
          throw "Could not fetch protocols; using cached version, if available"
        });
    }
    else {
      console.log("using cached protocols")
      callback(protocologist.protocols)
    }
  }
};

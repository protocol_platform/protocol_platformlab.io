let category = $('#category').data("category")

protocologist.filterByCategory(category, protocols => {
    displayProtocols(protocols)
});

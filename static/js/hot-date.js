
// See https://stackoverflow.com/questions/7641791/
function relativeDate(date) {

  var delta = Math.round((+new Date - date) / 1000);

  var minute = 60,
    hour  = minute * 60,
    day   = hour * 24,
    week  = day * 7,
    month = day * 30, // hmmm
    year  = day * 365;

  if (delta < 30) {
    return 'a moment ago';
  } else if (delta < minute) {
    return delta + ' seconds ago';
  } else if (delta < 2 * minute) {
    return 'a minute ago'
  } else if (delta < hour) {
    return Math.floor(delta / minute) + ' minutes ago';
  } else if (Math.floor(delta / hour) == 1) {
    return 'an hour ago'
  } else if (delta < day) {
    return Math.floor(delta / hour) + ' hours ago';
  } else if (delta < day * 2) {
    return 'yesterday';
  } else if (delta < week) {
    return Math.floor(delta / day) + ' days ago';
  } else if (delta < week * 2) {
    return 'last week'
  } else if (delta < month) {
    return Math.floor(delta / week) + ' weeks ago';
  } else if (delta < month * 2) {
    return 'last month'
  } else if (delta < year) {
    return Math.floor(delta / month) + ' months ago';
  } else if (delta < year * 2) {
    return 'last year'
  } else {
    return Math.floor(delta / year) + ' years ago';
  }
}

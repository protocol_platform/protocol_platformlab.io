'use strict';

const searchProtocolsEle = $('button#search-protocols')
searchProtocolsEle.click(filter)

$('input#query').keyup(event => {
  if (event.key === "Enter") {
    filter()
  } else {
    clearTimeout(window.filterTimeout)
    window.filterTimeout = setTimeout(filter, AUTOFILTER_DELAY)
  }
});

$('#protocol-filter input').click(event => {
  clearTimeout(window.filterTimeout)
  window.filterTimeout = setTimeout(filter, AUTOFILTER_DELAY)
});

searchProtocolsEle.click(filter)
$('button#filter-protocols').click(filter)

$('#toggle-filter').click(() => {
  $('#protocol-filter').toggle(500)
});

protocologist.getAll(protocols => {
  $("button #search-protocols-text").text('Search')
  $('button #search-protocols-spinner').hide()

  displayProtocols(protocols)
})

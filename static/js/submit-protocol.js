const NCT_ID_FORMAT = /^NCT\d{8}$/
const MIN_NCT_ID_CHARS = 5

function fillForm(fields) {
  jQuery.fn.extend({
    triggerRevalidation: function() {
      this.focusout()
    },
  })

  for (const id in fields) {
    element = $(`#${id}`)
    element.val(fields[id])
    element.triggerRevalidation()
    if (element.is('textarea')) {
      element.height( element[0].scrollHeight );
    }
  }
}

function displayNctIdError(message) {
  $('#nct-id-import-error').remove()
  $(`
    <label id='nct-id-import-error' class='error'>
      ${message}
      Please check the ID and try again.
    </label>
  `).insertAfter('#nct-id-help')
}

function displayNctIdFormatError(nctId) {
  displayNctIdError(
    `Sorry, your input '${nctId}' does not match the expected format 'NCT#########'.`
  )
}

function displayNctIdImportError(nctId) {
  displayNctIdError(
    `Sorry, we couldn't find a study with the ID '${nctId}'.`
  )
}

function displayNctIdImportSuccess(nctId, title) {
  $('#nct-id-import-success').remove()
  $(`
    <label id='nct-id-import-success' class='success'>
      <a href='https://clinicaltrials.gov/ct2/show/${nctId}'>
        ${title}
      </a>
      <span id='nct-id-import-success-icon'>
        <i class="fas fa-file-download"></i>
      </span>
    </label>
  `).insertAfter('#nct-id-help')
}

$('input#nct-id').on('keyup keypress', function (event) {
  if (event.target.value.length >= MIN_NCT_ID_CHARS) {
    $('#import').enable()
  } else {
    $('#import').prop('disabled', true)
  }
});

$('#import').click(function (event) {
  event.preventDefault();
  $('#nct-id-import-error').remove()

  const nctId = $('input#nct-id').val()

  if (!nctId.match(NCT_ID_FORMAT)) {
    displayNctIdFormatError(nctId);
    return;
  }

  clinicaltrials.getProtocol(nctId)
    .done(function(protocol) {
      if (protocol == null) {
        displayNctIdImportError(nctId)
      } else {
        fillForm(protocol)
        displayNctIdImportSuccess(nctId, protocol['brief-title'])
      }
    })
    .fail(function(protocol) {
      displayNctImportError(nctId)
    });
});

$("form#protocol").validate()

$("form#protocol").submit(function( event ) {
  event.preventDefault();
  if (!$('form#protocol').valid()) {
    return;
  }

  $("#submit-protocol-text").text("Submitting")
  $('#submit-protocol-spinner').show()

  airtable.submitProtocol(
    new FormData(this)
  ).done(function (data, url) {
    console.log(`successfully posted one record to ${url}`);
    alert("Thank you! Our team will review your submission and be in touch with you shortly.")
    window.location.replace("/")
  }).fail(function() {
    alert("Oops! Something went wrong and your protocol could not be saved. Must be something going around ... Please try again later, or email us if the problem persists.")
  }).always(function() {
    $("#submit-protocol-text").text("Submit")
    $("#submit-protocol-spinner").hide()
  });
});


function togglePatientData() {
  if ($("#sharePatientDataYes:checked").length > 0) {
    $("#sharedDataForms").show();
  } else {
    $("#sharedDataForms").hide();
  }
}

$("input[name='Share Patient Data']").change(_ => {
  togglePatientData();
});

$(document).ready(function() {
  let nct_id = request.params['nct-id']
  if (nct_id) {
    $('#nct-id').val(nct_id)
    $('#import').enable()
  }
  togglePatientData();
});


const AUTOFILTER_DELAY = 1500

const PROTOCOL_ROW_FIELDS = [
    'pi-name', 'pi-institution', 'status', 'intervention-arms'
]

const StatusClass = {
    'not yet enrolling': 'status-not-enrolling',
    'in progress': 'status-in-progress',
    'completed': 'status-completed'
}

function findByDataId(ele, id) {
    return ele.find(`[data-id='${id}']`);
}

function protocolRow(protocol) {
    let fulltextLink = `/protocol/?id=${protocol['ID']}`
    var row = Object.keys(protocol).reduce( (row, key) => {
        const id = key.toLowerCase().replace(/ +/, '-')
        if (PROTOCOL_ROW_FIELDS.includes(id)) {
            findByDataId(row, `${id}`).text(protocol[key])
        }
        return row
    }, $('#protocol-row-template').clone());

    let nctId = protocol['NCT ID'] || ''
    let title = protocol['Title'] + (nctId ? ` (${nctId})` : '')
    findByDataId(row, 'title')
        .text(title)
    findByDataId(row, 'fulltext-link').prop('href', fulltextLink).prop('title', title)
    findByDataId(row, 'created-at').text(
        `(listed ${relativeDate(Date.parse(protocol['__created_at__']))})`
    )
    if (protocol.hasOwnProperty('Standard Outcomes')) {
        protocol['Standard Outcomes'].forEach( outcome => {
                findByDataId(row, 'standard-outcomes').append(
                    safeHtml`<li>${outcome}</li>`
                )
            }
        )
    }
    populateCollaboratingTrialsField(row, protocol)

    row.addClass(StatusClass[protocol['Status']])
    row.removeAttr('id')
    return row
}

function populateCollaboratingTrialsField(row, protocol) {
    // Collaborating trials expanded/collapsed state
    let expanded = false;

    let titles = (protocol['Title (from Collaborating Protocols)'] || []).concat(
        (protocol['Title (from Collaborating External Trials)'] || []))

    // Returns the text for the expandable text based on the state
    function protocolText() {
        let protocolText = titles.length === 1 ? 'collaborating trial' : 'collaborating trials';
        let expandedText = expanded ?
            '' :
            '(click to expand)'
        return `${titles.length} ${protocolText} ${expandedText}`;
    }

    // Renders the collaborating trials field
    if (titles.length > 0) {
        let protocols = titles.map(
            title => {
                return safeHtml`<li>${title}</li>`
            }
        ).join("")
        let protocolFieldEle = findByDataId(row, 'collaborating-trials');
        protocolFieldEle.html(
            safeHtml`<span class="expandable-text">${protocolText()}</span>`
            + `<div style="display: none;"><ul>${protocols}</ul></div>`
        ).click(e => {
            e.preventDefault();
            expanded = !expanded;
            protocolFieldEle.find('div').toggle(500)
            protocolFieldEle.find('.expandable-text').text(protocolText())
        });
    }
}

function displayProtocols(protocols, statusText) {
    $('ul#protocols').empty()
    $('#protocols-count').text(statusText || `Showing ${thisMany(protocols)}`)
    protocols.forEach( protocol => {
        $('ul#protocols').append(protocolRow(protocol).show())
    });
}

function filter() {
    $("button #search-protocols-text").text("Searching")
    $('button #search-protocols-spinner').show()

    let delay = Math.floor((Math.random() * 1000) + 100)
    setTimeout(() => {
        const query  = { text: $('input#query').val() }

        let filter_ = {}
        if ($('#protocol-filter').is(':visible')) {
            filter_ = {
                'Standard Outcomes': $("input[name='Standard Outcomes']:checked")
                    .map((i, o) => o.value).get(),
                'Status':            $("input[name='Status']:checked")
                    .map((i, o) => o.value).get()
            }
        }

        protocologist.filter(query, filter_, matchingProtocols => {
            displayProtocols(
                matchingProtocols,
                `Found ${thisMany(matchingProtocols)} matching criteria`
            )
            $("button #search-protocols-text").text("Search")
            $('button #search-protocols-spinner').hide()
        });
    }, delay);
}

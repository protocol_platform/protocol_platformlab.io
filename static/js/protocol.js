const PROTOCOL_ROW_FIELDS = [
  'title',
  'nct-id',
  'study-type',
  'status',
  'pi-name', 'pi-institution',
  'co-pi-name', 'co-pi-institution',
  'intervention-arms',
  // 'standard-outcomes', 'custom-outcomes',
  'city', 'state', 'country',
  'eligibility-criteria',
  '__created_at__'
]

function piInfo(protocol) {
  let html = safeHtml`${protocol['PI Name']} (${protocol['PI Institution']})`
  if (protocol['Co-PI Name']) {
    html += '<br/>'
    html += safeHtml`${protocol['Co-PI Name']} (${protocol['Co-PI Institution']})`
  }
  return html
}

function studyType(protocol) {
  if (protocol['Study Type'] === 'randomized') {
    return 'randomized'
  } else {
    return 'single-arm / observational'
  }
}

function loc(protocol) {
  return [
    protocol['City'],
    protocol['State'],
    protocol['Country']
  ].filter(Boolean).join(', ')
}

function standardOutcomes(protocol) {
  let outcomes = protocol['Standard Outcomes'].map(o => safeHtml`<li>${o}</li>`).join(' ')
  return `<ul>${outcomes}</ul>`
}

function setCollaboratingTrials(protocol) {
    let ele = $('#collaborating-trials')
    if (protocol['Collaborating Protocols'] || protocol['Title (from Collaborating External Trials)']) {
        ele.html(collaboratingTrials(protocol));
    } else {
        ele.closest('.row').hide();
    }
}

function collaboratingTrials(protocol) {
  let protocolTitles = protocol['Title (from Collaborating Protocols)'] || [];
  let collabProtocols = (protocol['Collaborating Protocols'] || []).map(
    (id, i) => {
      let title = protocolTitles[i] || "";
      let href = `/protocol/?id=${id}`
      return safeHtml`<li><a href="${href}">${title}</a></li>`
    }
  )

  let links = protocol['Link (from Collaborating External Trials)'] || []
  let externalTrials = (protocol['Title (from Collaborating External Trials)'] || []).map(
    (title, i) => {
        let href = links[i] || "#";
        if (href === "#") {
            return safeHtml`<li>${title}</li>`
        } else {
            return safeHtml`<li><a href="${href}">${title}</a></li>`
        }
    }
  )
  return `<ul>${collabProtocols.concat(externalTrials).join("")}</ul>`
}

function outcomes(protocol) {
  return [
    standardOutcomes(protocol),
    protocol['Custom Outcomes'],
  ].filter(Boolean).join('<br />')
}

function namedAttachment(name) {
    function attachment(protocol) {
        const attachments = protocol[name].map(attachment =>
            safeHtml`<li><a href=${attachment.url}>${attachment.filename}</a></li>`
        ).join('')
        return `<ul>${attachments}</ul>`
    }
    return attachment;
}

function setOptionalField(ele, protocol, fieldName, decorator) {
  if (protocol[fieldName]) {
    ele.html(decorator(protocol));
  } else {
    ele.closest('.row').hide();
  }
}

function nctInfo(protocol) {
  let nctId = protocol['NCT ID']
  return safeHtml`
    <span id="nct-id">${nctId}</span>
    <br>
    <small class="nct-id-link text-muted text-small">
      view on
      <a href="https://clinicaltrials.gov/ct2/show/${nctId}" target="_blank">clinicaltrials.gov</a>
    </small>
    `
}

function collaborationRequestLink(protocol) {
  return encodeURI(`/request-collaboration/?protocol-id=${protocol['ID']}&title=${protocol['Title']}`)
}

$(document).ready(function() {
  let protocol_id = request.params['id']

  if (!protocol_id) {
    alert("Uh oh! Something went wrong and we couldn't find that protocol.")
    window.location.replace("/")
    throw `Could not find protocol {protocol_id}`
  }

  airtable.getProtocol(protocol_id)
    .done(protocol => {
      for (let [field_name, value] of Object.entries(protocol)) {
        const id = field_name.toLowerCase().replace(/ +/, '-')
        if (PROTOCOL_ROW_FIELDS.includes(id)) {
          $(`#${id}`).text(value || 'NA')
        }
      }

      $('#pi-info').html(piInfo(protocol))
      $('#location').text(loc(protocol))
      $('#study-type').text(studyType(protocol))
      $('#collaboration-request-link').prop('href',
        collaborationRequestLink(protocol)
      )
      $('#outcomes').html(outcomes(protocol))
      setOptionalField($('#nct-id'), protocol, 'NCT ID', nctInfo)
      setOptionalField($('#attachment'), protocol, 'Attachment', namedAttachment('Attachment'))
      setOptionalField($('#caseReportForm'), protocol, 'Case Report Form', namedAttachment('Case Report Form'))
      setOptionalField($('#patientConsentForms'), protocol, 'Patient Consent Forms', namedAttachment('Patient Consent Forms'))
      setOptionalField($('#dataDictionary'), protocol, 'Data Dictionary', namedAttachment('Data Dictionary'))
      setCollaboratingTrials(protocol)
      $('.spinner-border').hide()
    }).fail(() => {
      alert(`
        Oops! Something went wrong and we couldn't find
        a protocol matching '${protocol_id}'. Get in touch
        with us if you think this is a mistake.
      `.replace("\n", " "))
      // window.location.replace('/')
      throw `Could not find protocol '${protocol_id}'`
    });

});
